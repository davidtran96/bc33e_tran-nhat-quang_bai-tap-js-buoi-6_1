document.getElementById("result").style.display = "none";
function tinhTong() {
  var soX = document.getElementById("txt-x").value * 1,
    soN = document.getElementById("txt-n").value * 1,
    tong = 0;
  for (var i = 1; i <= soN; i++) {
    tong += Math.pow(soX, i);
  }
  document.getElementById("result").innerHTML = `Tổng: ${tong}`;
  document.getElementById("result").style.display = "block";
}
